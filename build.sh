#!/usr/bin/env bash
# exit on error
set -o errexit

poetry debug info
poetry install

python manage.py collectstatic --no-input
python manage.py migrate