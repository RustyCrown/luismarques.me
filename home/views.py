from django.shortcuts import render

def home(request):
    return render(request, 'home/home.html', {})

def publications(request):
    return render(request, 'home/publications.html', {})

def talks(request):
    return render(request, 'home/talks.html', {})

def software(request):
    return render(request, 'home/software.html', {})

def teaching(request):
    return render(request, 'home/teaching.html', {})

def contact(request):
    return render(request, 'home/contact.html', {})

def challenges(request):
    return render(request, 'home/challenges.html', {})

def test(request):
    return render(request, 'home/test.html', {})