from django.urls import path

from . import views

app_name = "home"

urlpatterns = [
    path('', views.home, name='home'),
    path('publications', views.publications, name='publications'),
    path('talks', views.talks, name='talks'),
    path('software', views.software, name='software'),
    path('teaching', views.teaching, name='teaching'),
    path('challenges', views.challenges, name='challenges'),
    path('contact', views.contact, name='contact'),

    path('test', views.test, name='test')
]